
  /*!
 * Custom job functions
 */
 
(function($) {
        // var data = 'http://localhost/ramrojagir/';
        // alert(data);      
    $(document).ready(function(){
        //show_product(); //call function show all product
          
        //function show all product
        // function show_product(){
        //     $.ajax({
        //         // type  : 'ajax',
        //         type  : "POST", // for live site
        //         url   : 'get_job_category',
        //         async : true,
        //         contentType: 'application/json',
        //         dataType : 'json',
        //         success : function(data){
        //             var html = '';
        //             var i;
        //             for(i=0; i<data.length; i++){
        //                 html += '<tr>'+
        //                         '<td>'+data[i].id+'</td>'+
        //                         '<td>'+data[i].name+'</td>'+
        //                         '<td>'+data[i].status+'</td>'+
        //                         '<td style="text-align:center;">'+
        //                             '<a href="javascript:void(0);" class="btn btn-info btn-sm item_edit" data-id="'+data[i].id+'" data-name="'+data[i].name+'" data-status="'+data[i].status+'">Edit</a>'+' '+
        //                             '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-id="'+data[i].id+'">Delete</a>'+
        //                         '</td>'+
        //                         '</tr>';
        //             }
        //             $('#show_data').html(html);
        //         }
 
        //     });
        // }
         

        //Save product
        $('#btn_save').on('click',function(){
            var name = $('#name').val();
            var icon = $('#icon').val();
            if (name < 0 || name > 100 || name === "") {
                // $("input#name").attr("required", true);
                // $('input').attr('required', true);   
                $('#name').attr('required', 'required');
                // $("input").prop('required',true);   
                // $("#name").css('border-color', 'red');
                // alert("Value should not be empty!");
                return false;
            }else{
                $('#name').removeAttr('required');                        
                // $('#name').css('border-color', '');                
                // $("input#name").attr("required", false);
            }
            if (icon < 0 || icon > 100 || icon === "") {
                // $("input#name").attr("required", true);
                // $('input').attr('required', true);   
                $('#icon').attr('required', 'required');
                // $("input").prop('required',true);   
                // $("#name").css('border-color', 'red');
                // alert("Value should not be empty!");
                return false;
            }else{
                $('#icon').removeAttr('required');                        
                // $('#name').css('border-color', '');                
                // $("input#name").attr("required", false);
            }
            // alert(name);
            $.ajax({
                type : "POST",
                url  : 'save_job_category',
                dataType : "JSON",
                data : {name:name,icon:icon},
                success: function(data){
                    $('[name="name"]').val("");
                    // $('#Modal_Add').modal('hide');
                    // show_product();
                    // alert('Category Added Successfully!');
                    toastr.success("Category Added Successfully!","Success Alert", {timeOut: 5000});
                    location.reload();
                    
                },
                error: function (jqXHR, status, err) {
                    if( jqXHR.responseText == 'permission_error'){
                        toastr.warning("Unauthorized Permission Found!","Warning Alert", {timeOut: 5000});
                    }else{
                        toastr.error("Category Added Error!","Error Alert", {timeOut: 5000});
                    }
                }
            });
            return false;
        });

        //get data for update record
        $('#show_data').on('click','.item_edit',function(){

            var id = $(this).data('id');
            var name = $(this).data('name');
            var icon = $(this).data('icon');
            var status = $(this).data('status');
            $('#Modal_Edit').modal('show');
            $('[name="id"]').val(id);
            $('[name="name_edit"]').val(name);
            $('[name="icon_edit"]').val(icon);
            $('[name="status"]').val(status);
        });

        //update record to database
         $('#btn_update').on('click',function(){
            var id = $('#id').val();
            var name = $('#name_edit').val();
            var icon = $('#icon_edit').val();
            var status = $('#status').val();
            $.ajax({
                type : "POST",
                url  : 'update_job_category',
                dataType : "JSON",
                data : {id:id , name:name,icon:icon, status:status},
                success: function(data){
                    $('[name="id"]').val("");
                    $('[name="name_edit"]').val("");
                    $('[name="icon_edit"]').val("");
                    $('[name="status"]').val("");
                    $('#Modal_Edit').modal('hide');
                    // show_product();
                    // alert('Category Updated Successfully!');
                    toastr.success("Category Updated Successfully!","Success Alert", {timeOut: 5000});
                    location.reload();
                },
                error: function (jqXHR, status, err) {
                    if( jqXHR.responseText == 'permission_error'){
                        toastr.warning("Unauthorized Permission Found!","Warning Alert", {timeOut: 5000});
                    }else{
                        toastr.error("Update Error!","Error Alert", {timeOut: 5000});
                    }
                    // alert("Already Exists");
                    $('#Modal_Edit').modal('hide');
                }
            });
            return false;
        });

        //get data for delete record
        $('#show_data').on('click','.item_delete',function(){
            var id = $(this).data('id');
             
            $('#Modal_Delete').modal('show');
            $('[name="id_delete"]').val(id);
        });

        //delete record to database
         $('#btn_delete').on('click',function(){
            var id = $('#id_delete').val();
            $.ajax({
                type : "POST",
                url  : 'delete_job_category',
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    $('[name="id"]').val("");
                    $('#Modal_Delete').modal('hide');
                    // show_product();
                    // alert('Category Deleted Successfully!');
                    toastr.success("Category Deleted Successfully!","Success Alert", {timeOut: 5000});
                    location.reload();
                },
                error: function (jqXHR, status, err) {
                    if( jqXHR.responseText == 'permission_error'){
                        toastr.warning("Unauthorized Permission Found!","Warning Alert", {timeOut: 5000});
                    }else{
                        toastr.error("Delete Error!","Error Alert", {timeOut: 5000});
                    }
                    $('#Modal_Delete').modal('hide');
                }
            });
            return false;
        });

         
    });
        
})(jQuery);