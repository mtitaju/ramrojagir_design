$(document).ready(function(){
    console.log('common.js loaded');

    $('.from_date').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd',
        maxDate: new Date(),
        onSelect: function () {
            var to_date = $('.to_date');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = to_date.datepicker('getDate');
            //difference in days. 86400 seconds in day, 1000 ms in second
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            
            //startDate.setDate(startDate.getDate() + 30);
            if (dt2Date == null || dateDiff < 0) {
                to_date.datepicker('setDate', minDate);
            }
            else if (dateDiff > 30){
                to_date.datepicker('setDate', startDate);
            }
            //sets to_date maxDate to the last day of 30 days window
            //to_date.datepicker('option', 'maxDate', startDate);
            to_date.datepicker('option', 'minDate', minDate);
        }
    });
    $('.to_date').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd',
        maxDate: new Date(),
    });

    $('.datepicker').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd',
        maxDate: new Date(),
    });
    
});