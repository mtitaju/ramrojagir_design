// $(document).ready(function(){
//     $('#types').on('change', function() {
//       if ( this.value == 'cgpa')
//       {
//         $(".cgpa").show();
//         $(".percentage").hide();
//       }
//       else
//       {
//         $(".percentage").show();
//         $(".cgpa").hide();
//       }
//     });
// });
// ADDED NEW
// modal cancle class
jQuery(document).on('click', '.close.btn-default', function () {
    // alert('closing');
    $("#education_add_modal").slideUp("slow");
    $("#experience_add_modal").slideUp("slow");
    $("#involvement_add_modal").slideUp("slow");
    $("#training_add_modal").slideUp("slow");
    $("#skill_add_modal").slideUp("slow");
    $("#language_add_modal").slideUp("slow");
    $("#driving_add_modal").slideUp("slow");
    $("#publication_add_modal").slideUp("slow");
});


// Education.
// to view education detail.
viewEducationData();
function viewEducationData(){
	var action ="loadData";
	$.ajax({
		url:"getEducation",
		method:"POST",
		data:{action:action},
		success:function(data){
			$("#view_education_data").html(data);
		}
	});
}
// when add aducation button fire, open modal and sets some action.
$( "#create_education_btn" ).on('click', function (e) {
    $('.careerfy-add-popup:not(#education_add_modal)').slideUp('slow');
    $( this ).parents('#education_add_wrap').find('#education_add_modal').slideToggle( "slow", function() {});
    // if modal is opened.
    e.preventDefault();
    $("#education_add_form")[0].reset();
    // $("#uploaded_hidden_image").html('');
    $("#form-title").text('Create Education Resume');
    $("#action").val('create');
    $("#submit").val('create');
    return false;
});
// when education add button click.
$( "#education_add_form" ).on('submit', function (e) {
    e.preventDefault();
    var institute = $("#institute").val();
    var level = $("#level").val();
    var area_of_study = $("#area_of_study").val();
    var division = $("#division").val();
    var employee_id = $("#employee_id").val();
    var passed_year = $("#passed_year").val();
    var obtained_marks = $("#obtained_marks").val();
    var address = $("#address").val();
    var description = $("#description").val();
    
    // else if(area_of_study ==''){
    //     swal({
    //             title:"Area of Study Field Required",
    //             icon:"warning"
    //         });
    // }
    
    
    if (institute == '') {
        swal({
                title:"Institute Field Required",
                icon:"warning"
            });
    }else if(level == ''){
        swal({
                title:"Education Level Field Required",
                icon:"warning"
            });
    }else if(division ==''){
        swal({
                title:"Division Field Required",
                icon:"warning"
            });
    }else if(passed_year ==''){
        swal({
                title:"Passed Year Field Required",
                icon:"warning"
            });
    }else if(obtained_marks ==''){
        swal({
                title:"Obtained Marks Field Required",
                icon:"warning"
            });
    }else if(address ==''){
        swal({
                title:"Address Field Required",
                icon:"warning"
            });
    }else if(employee_id ==''){
        swal({
                title:"Employee ID Field Required",
                icon:"warning"
            });
    }/*else if(description ==''){
        swal({
                title:"Description Field Required",
                icon:"warning"
            });
    }*/else{
        var formData = new FormData($('#education_add_form')[0]);
        $.ajax({
            url:"add_update_education_resume",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            // dataType: "JSON",
            success:function(data){
                swal({
                        title:"Education Added successfull",
                        icon:"success"
                    });
                if (data.trim()=='created') {
                    swal({
                        title:"Education Added successfull",
                        icon:"success"
                    });
                    $("#education_add_modal").slideUp("slow");
                    $("#education_add_form")[0].reset();
                }
                
                if (data.trim()=='update') {
                    swal({
                        title:"Education Update successfull",
                        icon:"success"
                    });
                    $("#education_add_modal").slideUp("slow");
                    $("#education_add_form")[0].reset();
                }
                viewEducationData();
            }
        });
    }
});
// when update button is fire or click.
$(document).on("click","#editBtnId",function(e){
	e.preventDefault();
	var editBtnId = $(this).attr('data-editBtnId');
	var action = 'getSingleData';
	$.ajax({
		url:"getSingleEducation",
		method:"POST",
		data:{editBtnId:editBtnId,action:action},
		dataType:"json",
		success:function(data){
        //$("#create_form_modal").modal('show');
        //modal show.
        // $( this ).parents('#education_add_wrap').find('#education_add_modal').slideToggle( "slow", function() {});
        // if modal is opened.
        // e.preventDefault();
        $('.careerfy-add-popup:not(#education_add_modal)').slideUp('slow');
        $('#education_add_modal').slideToggle( "slow", function() {});
		$("#institute").val(data.institute);
		$("#level").val(data.level);
		$("#area_of_study").val(data.area_of_study);
		//$("#division").val(data.division);
		var division = data.division;
        if(division === 'first'){
            $('#division option[value=first]').prop('selected', 'selected').change();
        } else if(division === 'second'){
            $('#division option[value=second]').prop('selected', 'selected').change();
        } else if(division === 'third'){
            $('#division option[value=third]').prop('selected', 'selected').change();
        } else if(division === 'pass'){
            $('#division option[value=pass]').prop('selected', 'selected').change();
        } 
		$("#passed_year").val(data.passed_year);
		// for %/ cgpa determine.
        var types = data.obtained_marks;
        /* if(types.indexOf('.') !== -1){
            $('#types option[value=cgpa]').prop('selected', 'selected').change();
        }else{
            $('#types option[value=percentage]').prop('selected', 'selected').change();
        } */
        if(data.marks_type=='cgpa'){
            $('#types option[value=cgpa]').prop('selected', 'selected').change();
        }else if(data.marks_type=='percentage'){
            $('#types option[value=percentage]').prop('selected', 'selected').change();
        }
		$("#obtained_marks").val(data.obtained_marks);
		$("#address").val(data.address);
		$("#description").val(data.description);
		//$("#uploaded_image").val(data.image);
		//$("#uploaded_hidden_image").html(data.uploaded_hidden_image);
		$("#form-title").text('Education Resume Update');
		$("#action").val('update');
		$("#submit").val('update');
		$("#update_id").val(editBtnId);
		}
	});
});
// delete education.
$(document).on("click","#delteBtnId",function(e){
	e.preventDefault();
    var delteBtnId = $(this).attr('data-delteBtnId');
	var action ='delete';
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
     },
      function(isConfirm){
         if (isConfirm) {
        	$.ajax({
        		url:"delete_education_resume",
        		method:"POST",
        		data:{delteBtnId:delteBtnId,action:action}, //deleteImg:deleteImg,
        		success:function(data){
        			if (data.trim()=='deleted') {
        				swal({
        					title:"Education deleted successfull",
        					icon:"success"
        				});
        				viewEducationData();
        			}
        		}
        	});
        } else {
                swal("Canceled", "Your data is safe :)", "error");
                return false;
        }
    });
});


// Experience
// to view experience detail.
viewExperienceData();
function viewExperienceData(){
	var action ="loadData";
	$.ajax({
		url:"getExperience",
		method:"POST",
		data:{action:action},
		success:function(data){
			$("#view_experience_data").html(data);
		}
	});
}
// when add aducation button fire, open modal and sets some action.
$( "#create_experience_btn" ).on('click', function (e) {
    $('.careerfy-add-popup:not(#experience_add_modal)').slideUp('slow');
    $( this ).parents('#experience_add_wrap').find('#experience_add_modal').slideToggle( "slow", function() {});
    // if modal is opened.
    e.preventDefault();
    $("#experience_add_form")[0].reset();
    $("#experience_form-title").text('Create Experience Resume');
    $("#experience_action").val('create');
    $("#experience_submit").val('create');
    return false;
});
// when experience add button click.
$( "#experience_add_form" ).on('submit', function (e) {
    e.preventDefault();
    var organization = $("#organization").val();
    var job_title = $("#job_title").val();
    var job_position = $("#job_position").val();
    // var responsbility = $("#responsbility").val();
    var employee_id = $("#experience_employee_id").val();
    var join_date = $("#experience_join_date").val();
    var leave_date = $("#experience_leave_date").val();
    var address = $("#experience_address").val();
    var description = $("#experience_description").val();
    if (organization == '') {
        swal({
                title:"Organization Field Required",
                icon:"warning"
            });
    }else if(job_title == ''){
        swal({
                title:"Job_title Field Required",
                icon:"warning"
            });
    }
    // else if(job_position ==''){
    //     swal({
    //             title:"Job Position Field Required",
    //             icon:"warning"
    //         });
    // }
    // else if(responsbility ==''){
    //     swal({
    //             title:"Job Responsbility Field Required",
    //             icon:"warning"
    //         });
    // }
    else if(join_date ==''){
        swal({
                title:"Join Date Field Required",
                icon:"warning"
            });
    }/*else if(leave_date ==''){
        swal({
                title:"Resign Marks Field Required",
                icon:"warning"
            });
    }*/ else if(address ==''){
        swal({
                title:"Address Field Required",
                icon:"warning"
            });
    }else if(employee_id ==''){
        swal({
                title:"Employee ID Field Required",
                icon:"warning"
            });
    }/*else if(description ==''){
        swal({
                title:"Description Field Required",
                icon:"warning"
            });
    }*/else{
        var formData = new FormData($('#experience_add_form')[0]);
        $.ajax({
            url:"add_update_experience_resume",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            // dataType: "JSON",
            success:function(data){
                swal({
                        title:"Experience Added successfull",
                        icon:"success"
                    });
                if (data.trim()=='created') {
                    swal({
                        title:"Experience Added successfull",
                        icon:"success"
                    });
                    $("#experience_add_modal").slideUp("slow");
                    $("#experience_add_form")[0].reset();
                }
                
                if (data.trim()=='update') {
                    swal({
                        title:"Experience Update successfull",
                        icon:"success"
                    });
                    $("#experience_add_modal").slideUp("slow");
                    $("#experience_add_form")[0].reset();
                }
                viewExperienceData();
            }
        });
    }
});
// when update button is fire or click.
$(document).on("click","#editExperienceBtnId",function(e){
	e.preventDefault();
	var editBtnId = $(this).attr('data-editBtnId');
	var action = 'getSingleData';
	$.ajax({
		url:"getSingleExperience",
		method:"POST",
		data:{editBtnId:editBtnId,action:action},
		dataType:"json",
		success:function(data){
            $('.careerfy-add-popup:not(#experience_add_modal)').slideUp('slow');
	        //modal show.
	        $('#experience_add_modal').slideToggle( "slow", function() {});
			$("#organization").val(data.organization);
			$("#job_title").val(data.job_title);
			$("#job_position").val(data.job_position);
// 			$("#responsbility").val(data.responsbility);
			$("#experience_join_date").val(data.join_date);
			$("#experience_leave_date").val(data.leave_date);
			$("#experience_address").val(data.address);
			$("#experience_description").val(data.description);
			$("#experience_form-title").text('Experience Resume Update');
			$("#experience_action").val('update');
			$("#experience_submit").val('update');
			$("#experience_update_id").val(editBtnId);
		}
	});
});
// delete experience.
$(document).on("click","#delteExperienceBtnId",function(e){
	e.preventDefault();
    var delteBtnId = $(this).attr('data-delteBtnId');
	var action ='delete';
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm){
        if (isConfirm) {
        	$.ajax({
        		url:"delete_experience_resume",
        		method:"POST",
        		data:{delteBtnId:delteBtnId,action:action}, //deleteImg:deleteImg,
        		success:function(data){
        			if (data.trim()=='deleted') {
        				swal({
        					title:"Experience deleted successfull",
        					icon:"success"
        				});
        				viewExperienceData();
        			}
        		}
        	});
        } else {
                swal("Canceled", "Your data is safe :)", "error");
                return false;
        }
    });
});


// INVOLVEMENTS
// to view involvement detail.
viewInvolvementData();
function viewInvolvementData(){
    var action ="loadData";
    $.ajax({
        url:"getInvolvements",
        method:"POST",
        data:{action:action},
        success:function(data){
            $("#view_involvement_data").html(data);
        }
    });
}
// when add involvement button fire, open modal and sets some action.
$( "#create_involvement_btn" ).on('click', function (e) {
    $('.careerfy-add-popup:not(#involvement_add_modal)').slideUp('slow');
    $( this ).parents('#involvement_add_wrap').find('#involvement_add_modal').slideToggle( "slow", function() {});
    // if modal is opened.
    e.preventDefault();
    $("#involvement_add_form")[0].reset();
    $("#form-title").text('Create Involvement Resume');
    $("#involvement_action").val('create');
    $("#involvement_submit").val('create');
    return false;
});
// when involvement add button click.
$( "#involvement_add_form" ).on('submit', function (e) {
    e.preventDefault();
    var organization = $("#involvement_organization").val();
    var job_title = $("#involvement_job_title").val();
    var job_position = $("#involvement_job_position").val();
    var type = $("#involvement_type").val();
    var employee_id = $("#involvement_employee_id").val();
    var address = $("#involvement_address").val();
    var description = $("#involvement_description").val();
    if (organization == '') {
        swal({
                title:"Organization Field Required",
                icon:"warning"
            });
    }
    // else if(job_position ==''){
    //     swal({
    //             title:"Job Position Field Required",
    //             icon:"warning"
    //         });
    // }
    else if(job_title == ''){
        swal({
                title:"Job_title Field Required",
                icon:"warning"
            });
    }else if(type ==''){
        swal({
                title:"Involvement Type Field Required",
                icon:"warning"
            });
    }else if(address ==''){
        swal({
                title:"Address Field Required",
                icon:"warning"
            });
    }else if(employee_id ==''){
        swal({
                title:"Employee ID Field Required",
                icon:"warning"
            });
    }/*else if(description ==''){
        swal({
                title:"Description Field Required",
                icon:"warning"
            });
    }*/else{
        var formData = new FormData($('#involvement_add_form')[0]);
        $.ajax({
            url:"add_update_involvements_resume",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            // dataType: "JSON",
            success:function(data){
                swal({
                        title:"Involvement Added successfull",
                        icon:"success"
                    });
                if (data.trim()=='created') {
                    swal({
                        title:"Involvement Added successfull",
                        icon:"success"
                    });
                    $("#involvement_add_modal").slideUp("slow");
                    $("#involvement_add_form")[0].reset();
                }
                
                if (data.trim()=='update') {
                    swal({
                        title:"Involvement Update successfull",
                        icon:"success"
                    });
                    $("#involvement_add_modal").slideUp("slow");
                    $("#involvement_add_form")[0].reset();
                }
                viewInvolvementData();
            }
        });
    }
});
// when update button is fire or click.
$(document).on("click","#editInvolvementBtnId",function(e){
    e.preventDefault();
    var editBtnId = $(this).attr('data-editBtnId');
    var action = 'getSingleData';
    $.ajax({
        url:"getSingleInvolvements",
        method:"POST",
        data:{editBtnId:editBtnId,action:action},
        dataType:"json",
        success:function(data){   
            $('.careerfy-add-popup:not(#involvement_add_modal)').slideUp('slow');          
            //modal show.
            $('#involvement_add_modal').slideToggle( "slow", function() {});
            $("#involvement_organization").val(data.organization);
            $("#involvement_job_title").val(data.job_title);
            $("#involvement_job_position").val(data.job_position);
            $("#involvement_type").val(data.type);
            $("#involvement_address").val(data.address);
            $("#involvement_description").val(data.description);
            $("#involvement_form-title").text('Involvement Resume Update');
            $("#involvement_action").val('update');
            $("#involvement_submit").val('update');
            $("#involvement_update_id").val(editBtnId);
        }
    });
});
// delete Involvement.
$(document).on("click","#delteInvolvementBtnId",function(e){
    e.preventDefault();
    var delteBtnId = $(this).attr('data-delteBtnId');
    var action ='delete';
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm){
        if (isConfirm) {
            $.ajax({
                url:"delete_involvements_resume",
                method:"POST",
                data:{delteBtnId:delteBtnId,action:action}, //deleteImg:deleteImg,
                success:function(data){
                    if (data.trim()=='deleted') {
                        swal({
                            title:"Involvement deleted successfull",
                            icon:"success"
                        });
                        viewInvolvementData();
                    }
                }
            });
        } else {
                swal("Cancelled", "Your data is safe :)", "error");
                return false;
        }
    });
});



// TRAINING
// to view training detail.
viewTrainingData();
function viewTrainingData(){
    var action ="loadData";
    $.ajax({
        url:"getTraining",
        method:"POST",
        data:{action:action},
        success:function(data){
            $("#view_training_data").html(data);
        }
    });
}
// when add training button fire, open modal and sets some action.
$( "#create_training_btn" ).on('click', function (e) {
    $('.careerfy-add-popup:not(#training_add_modal)').slideUp('slow');
    $( this ).parents('#training_add_wrap').find('#training_add_modal').slideToggle( "slow", function() {});
    // if modal is opened.
    e.preventDefault();
    $("#training_add_form")[0].reset();
    $("#form-title").text('Create training Resume');
    $("#training_action").val('create');
    $("#training_submit").val('create');
    return false;
});

// when Training add button click.
$( "#training_add_form" ).on('submit', function (e) {
    e.preventDefault();
    var training_name = $("#training_name").val();
    var duration = $("#training_duration").val();
    var employee_id = $("#training_employee_id").val();
    var start_date = $("#training_start_date").val();
    var end_date = $("#training_end_date").val();
    var description = $("#training_description").val();
    if (training_name == '') {
        swal({
                title:"Training_name Field Required",
                icon:"warning"
            });
    }
    // else if(duration == ''){
    //     swal({
    //             title:"Training Duration Field Required",
    //             icon:"warning"
    //         });
    // }
    else if(start_date ==''){
        swal({
                title:"Start Date Field Required",
                icon:"warning"
            });
    }
    // else if(end_date ==''){
    //     swal({
    //             title:"End Date Field Required",
    //             icon:"warning"
    //         });
    // }
    else if(employee_id ==''){
        swal({
                title:"Employee ID Field Required",
                icon:"warning"
            });
    }
    /*else if(description ==''){
        swal({
                title:"Description Field Required",
                icon:"warning"
            });
    }*/else{
        var formData = new FormData($('#training_add_form')[0]);
        $.ajax({
            url:"add_update_training_resume",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            // dataType: "JSON",
            success:function(data){
                swal({
                        title:"Training Added successfull",
                        icon:"success"
                    });
                if (data.trim()=='created') {
                    swal({
                        title:"Training Added successfull",
                        icon:"success"
                    });
                    $("#training_add_modal").slideUp("slow");
                    $("#training_add_form")[0].reset();
                }
                
                if (data.trim()=='update') {
                    swal({
                        title:"Training Update successfull",
                        icon:"success"
                    });
                    $("#training_add_modal").slideUp("slow");
                    $("#training_add_form")[0].reset();
                }
                viewTrainingData();
            }
        });
    }
});
// when update button is fire or click.
$(document).on("click","#editTrainingBtnId",function(e){
    e.preventDefault();
    var editBtnId = $(this).attr('data-editBtnId');
    var action = 'getSingleData';
    $.ajax({
        url:"getSingleTraining",
        method:"POST",
        data:{editBtnId:editBtnId,action:action},
        dataType:"json",
        success:function(data){  
            $('.careerfy-add-popup:not(#training_add_modal)').slideUp('slow');           
            //modal show.
            $('#training_add_modal').slideToggle( "slow", function() {});
            $("#training_name").val(data.training_name);
            $("#training_duration").val(data.duration);
            $("#training_start_date").val(data.start_date);
            $("#training_end_date").val(data.end_date);
            $("#training_description").val(data.description);
            $("#training_form-title").text('Training Resume Update');
            $("#training_action").val('update');
            $("#training_submit").val('update');
            $("#training_update_id").val(editBtnId);
        }
    });
});
// delete Training.
$(document).on("click","#delteTrainingBtnId",function(e){
    e.preventDefault();
    var delteBtnId = $(this).attr('data-delteBtnId');
    var action ='delete';
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm){
        if (isConfirm) {
            $.ajax({
                url:"delete_training_resume",
                method:"POST",
                data:{delteBtnId:delteBtnId,action:action}, //deleteImg:deleteImg,
                success:function(data){
                    if (data.trim()=='deleted') {
                        swal({
                            title:"Training deleted successfull",
                            icon:"success"
                        });
                        viewTrainingData();
                    }
                }
            });
        } else {
                swal("Cancelled", "Your data is safe :)", "error");
                return false;
        }
    });
});


// SKILL
// to view skill detail.
viewSkillData();
function viewSkillData(){
    var action ="loadData";
    $.ajax({
        url:"getSkill",
        method:"POST",
        data:{action:action},
        success:function(data){
            $("#view_skill_data").html(data);
        }
    });
}
// when add training button fire, open modal and sets some action.
$( "#create_skill_btn" ).on('click', function (e) {
    $('.careerfy-add-popup:not(#skill_add_modal)').slideUp('slow');
    $( this ).parents('#skill_add_wrap').find('#skill_add_modal').slideToggle( "slow", function() {});
    // if modal is opened.
    e.preventDefault();
    $("#skill_add_form")[0].reset();
    $("#form-title").text('Create Skill Resume');
    $("#skill_action").val('create');
    $("#skill_submit").val('create');
    return false;
});
// when Training add button click.
$( "#skill_add_form" ).on('submit', function (e) {
    e.preventDefault();
    var skill_name = $("#skill_name").val();
    var skill_field = $("#skill_field").val();
    var employee_id = $("#skill_employee_id").val();
    var description = $("#skill_description").val();
    if (skill_name == '') {
        swal({
                title:"Skill Name Field Required",
                icon:"warning"
            });
    }else if(skill_field == ''){
        swal({
                title:"Skill Field Required",
                icon:"warning"
            });
    }else if(employee_id ==''){
        swal({
                title:"Employee ID Field Required",
                icon:"warning"
            });
    }/*else if(description ==''){
        swal({
                title:"Description Field Required",
                icon:"warning"
            });
    }*/else{
        var formData = new FormData($('#skill_add_form')[0]);
        $.ajax({
            url:"add_update_skill_resume",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            // dataType: "JSON",
            success:function(data){
                swal({
                        title:"Skill Added successfull",
                        icon:"success"
                    });
                if (data.trim()=='created') {
                    swal({
                        title:"Skill Added successfull",
                        icon:"success"
                    });
                    $("#skill_add_modal").slideUp("slow");
                    $("#skill_add_form")[0].reset();
                }
                
                if (data.trim()=='update') {
                    swal({
                        title:"Skill Update successfull",
                        icon:"success"
                    });
                    $("#skill_add_modal").slideUp("slow");
                    $("#skill_add_form")[0].reset();
                }
                viewSkillData();
            }
        });
    }
});
// when update button is fire or click.
$(document).on("click","#editSkillBtnId",function(e){
    e.preventDefault();
    var editBtnId = $(this).attr('data-editBtnId');
    var action = 'getSingleData';
    $.ajax({
        url:"getSingleSkill",
        method:"POST",
        data:{editBtnId:editBtnId,action:action},
        dataType:"json",
        success:function(data){  
            $('.careerfy-add-popup:not(#skill_add_modal)').slideUp('slow');           
            //modal show.
            $('#skill_add_modal').slideToggle( "slow", function() {});
            $("#skill_name").val(data.skill_name);
            $("#skill_field").val(data.skill_field);
            $("#skill_description").val(data.description);
            $("#skill_form-title").text('Skill Resume Update');
            $("#skill_action").val('update');
            $("#skill_submit").val('update');
            $("#skill_update_id").val(editBtnId);
        }
    });
});
// delete Training.
$(document).on("click","#delteSkillBtnId",function(e){
    e.preventDefault();
    var delteBtnId = $(this).attr('data-delteBtnId');
    var action ='delete';
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm){
        if (isConfirm) {
            $.ajax({
                url:"delete_skill_resume",
                method:"POST",
                data:{delteBtnId:delteBtnId,action:action}, //deleteImg:deleteImg,
                success:function(data){
                    if (data.trim()=='deleted') {
                        swal({
                            title:"Training deleted successfull",
                            icon:"success"
                        });
                        viewSkillData();
                    }
                }
            });
        } else {
                swal("Cancelled", "Your data is safe :)", "error");
                return false;
        }
    });
});


// Language
// to view Language detail.
viewLanguageData();
function viewLanguageData(){
    var action ="loadData";
    $.ajax({
        url:"getLanguage",
        method:"POST",
        data:{action:action},
        success:function(data){
            $("#view_langauge_data").html(data);
        }
    });
}
// when add aducation button fire, open modal and sets some action.
$( "#create_language_btn" ).on('click', function (e) {
    $('.careerfy-add-popup:not(#language_add_modal)').slideUp('slow');
    $( this ).parents('#language_add_wrap').find('#language_add_modal').slideToggle( "slow", function() {});
    // if modal is opened.
    e.preventDefault();
    $("#language_add_form")[0].reset();
    $("#form-title").text('Create Langauge Resume');
    $("#language_action").val('create');
    $("#language_submit").val('create');
    return false;
});
// when Language add button click.
$( "#language_add_form" ).on('submit', function (e) {
    e.preventDefault();
    var language_name = $("#language_name").val();
    var reading = $("#reading").val();
    var writing = $("#writing").val();
    var speaking = $("#speaking").val();
    // var language_proficiency_level = $("#language_proficiency_level").val();
    var language_description = $("#language_description").val();
    if (language_name == '') {
        swal({
                title:"Language Name Field Required",
                icon:"warning"
            });
    }else if(reading == ''){
        swal({
                title:"Reading Rating Required",
                icon:"warning"
            });
    }else if(writing == ''){
        swal({
                title:"Writing Rating Required",
                icon:"warning"
            });
    }else if(speaking == ''){
        swal({
                title:"Speaking Rating Required",
                icon:"warning"
            });
    }/*else if(language_proficiency_level == ''){
        swal({
                title:"Langauge Proficiency Field Required",
                icon:"warning"
            });
    }*//*else if(description ==''){
        swal({
                title:"Description Field Required",
                icon:"warning"
            });
    }*/else{
        var formData = new FormData($('#language_add_form')[0]);
        $.ajax({
            url:"add_update_language_resume",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            // dataType: "JSON",
            success:function(data){
                swal({
                        title:"Language Added successfull",
                        icon:"success"
                    });
                if (data.trim()=='created') {
                    swal({
                        title:"Language Added successfull",
                        icon:"success"
                    });
                    $("#language_add_modal").slideUp("slow");
                    $("#language_add_form")[0].reset();
                }
                
                if (data.trim()=='update') {
                    swal({
                        title:"Language Update successfull",
                        icon:"success"
                    });
                    $("#language_add_modal").slideUp("slow");
                    $("#language_add_form")[0].reset();
                }
                viewLanguageData();
            }
        });
    }
});
// when update button is fire or click.
$(document).on("click","#editLanguageBtnId",function(e){
    e.preventDefault();
    var editBtnId = $(this).attr('data-editBtnId');
    var action = 'getSingleData';
    $.ajax({
        url:"getSingleLanguage",
        method:"POST",
        data:{editBtnId:editBtnId,action:action},
        dataType:"json",
        success:function(data){    
            $('.careerfy-add-popup:not(#language_add_modal)').slideUp('slow');         
            //modal show.
            $('#language_add_modal').slideToggle( "slow", function() {});
            $("#language_name").val(data.language_name);
            $("#reading").val(data.reading);
            $("#writing").val(data.writing);
            $("#speaking").val(data.speaking);
            $("#listening").val(data.listening);
            // $("#language_proficiency_level").val(data.language_proficiency_level);
            $("#language_description").val(data.description);
            $("#form-title").text('Language Resume Update');
            $("#language_action").val('update');
            $("#language_submit").val('update');
            $("#language_update_id").val(editBtnId);
        }
    });
});
// delete Language.
$(document).on("click","#delteLanguageBtnId",function(e){
    e.preventDefault();
    var delteBtnId = $(this).attr('data-delteBtnId');
    var action ='delete';
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm){
        if (isConfirm) {
            $.ajax({
                url:"delete_language_resume",
                method:"POST",
                data:{delteBtnId:delteBtnId,action:action}, //deleteImg:deleteImg,
                success:function(data){
                    if (data.trim()=='deleted') {
                        swal({
                            title:"Language deleted successfull",
                            icon:"success"
                        });
                        viewLanguageData();
                    }
                }
            });
        } else {
                swal("Cancelled", "Your data is safe :)", "error");
                return false;
        }
    });
});


// Driving License
// to view license detail.
viewLicenseData();
function viewLicenseData(){
    var action ="loadData";
    $.ajax({
        url:"getLicense",
        method:"POST",
        data:{action:action},
        success:function(data){
            $("#view_license_data").html(data);
        }
    });
}
// when add license button fire, open modal and sets some action.
$("#create_driving_btn" ).on('click', function (e) {
// $("#create_driving_btn").bind("click", (function (e) {
    $('.careerfy-add-popup:not(#driving_add_modal)').slideUp('slow');
    $( this ).parents('#driving_add_wrap').find('#driving_add_modal').slideToggle( "slow", function() {});
    // if modal is opened.
    e.preventDefault();
    $("#driving_add_form")[0].reset();
    $("#form-title").text('Create License Resume');
    $("#license_action").val('create');
    $("#license_submit").val('create');
    return false;
});
// when License add button click.
// $( "#driving_add_form" ).on('click', function (e) {
$( "#license_submit" ).on('click', function (e) {
    e.preventDefault();
    // array.
    var license_data = [];
    $.each($("input[name='license_name']:checked"), function(){    
        license_data.push($(this).val());
    });
    // assing license data( array )
    var license_name = license_data; //$("#license_name").val();
    var license_no = $("#license_no").val();
    var expiry_date = $("#expiry_date").val();
    var license_update_id = $("#license_update_id").val();
    var license_employee_id = $("#license_employee_id").val();
    var license_action = $("#license_action").val();
    if (license_name.length <= 0) {
        swal({
                title:"At Least One License Item Required",
                icon:"warning"
            });
    }else if(license_no == ''){
        swal({
                title:"License No. Required",
                icon:"warning"
            });
    }else if(expiry_date == ''){
        swal({
                title:"License Expiry Date Required",
                icon:"warning"
            });
    }/*else if(description ==''){
        swal({
                title:"Description Field Required",
                icon:"warning"
            });
    }*/else{
        // var formData = new FormData($('#driving_add_form')[0]);
        $.ajax({
            url:"add_update_license_resume",
            type: "POST",
            // data: formData,
            // contentType: false,
            // processData: false,
            
            data:{license_name:license_name,license_no:license_no,expiry_date:expiry_date,license_update_id:license_update_id,license_employee_id:license_employee_id,license_action:license_action},
            // data: $('#driving_add_form').serialize(),
            dataType:"text",
            success:function(data){
                swal({
                        title:"License Added successfull",
                        icon:"success"
                });
                if (data.trim()=='created') {
                    swal({
                        title:"License Added successfull",
                        icon:"success"
                    });
                    $("#driving_add_modal").slideUp("slow");
                    $("#driving_add_form")[0].reset();
                }
                
                if (data.trim()=='update') {
                    swal({
                        title:"License Update successfull",
                        icon:"success"
                    });
                    $("#driving_add_modal").slideUp("slow");
                    $("#driving_add_form")[0].reset();
                }
                
                if (data.trim()=='exists') {
                    swal({
                        title:"License already exist, to add please remove it.",
                        icon:"error"
                    });
                    $("#driving_add_modal").slideUp("slow");
                    $("#driving_add_form")[0].reset();
                }
                // swal("Cancelled", "Your data is safe :)", "error");
                // return false;
                
                
                viewLicenseData();
            },
            
            error: function (jqXHR, textStatus, errorThrown) {  console.log(jqXHR); console.log(textStatus); console.log(errorThrown); alert('error'); }

        });
    }
});
// when update button is fire or click.
$(document).on("click","#editLicenseBtnId",function(e){
    e.preventDefault();
    var editBtnId = $(this).attr('data-editBtnId');
    var action = 'getSingleData';
    $.ajax({
        url:"getSingleLicense",
        method:"POST",
        data:{editBtnId:editBtnId,action:action},
        dataType:"json",
        success:function(data){ 
            $('.careerfy-add-popup:not(#driving_add_modal)').slideUp('slow');                     
            //modal show.
            $('#driving_add_modal').slideToggle( "slow", function() {});
            $("#license_name").val(data.license_name);
            $("#license_no").val(data.license_no);
            $("#expiry_date").val(data.expiry_date);
            // $("#description").val(data.description);
            $("#form-title").text('License Resume Update');
            $("#license_action").val('update');
            $("#license_submit").val('update');
            $("#license_update_id").val(editBtnId);
        }
    });
});
// delete License.
$(document).on("click","#delteLicenseBtnId",function(e){
    e.preventDefault();
    var delteBtnId = $(this).attr('data-delteBtnId');
    var action ='delete';
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm){
        if (isConfirm) {
            $.ajax({
                url:"delete_license_resume",
                method:"POST",
                data:{delteBtnId:delteBtnId,action:action}, //deleteImg:deleteImg,
                success:function(data){
                    if (data.trim()=='deleted') {
                        swal({
                            title:"License deleted successfull",
                            icon:"success"
                        });
                        viewLicenseData();
                    }
                }
            });
        } else {
                swal("Cancelled", "Your data is safe :)", "error");
                return false;
        }
    });
});

var obtained_marks='';
$(document).on('focus','#obtained_marks',function(){
    obtained_marks = $(this).val();
});
$(document).on('change','#types',function(){
    if(validate_marks()==false){
        $('#obtained_marks').val('');
        obtained_marks='';
    }
});
$(document).on('input','#obtained_marks',function(){
    var input_value = $('#obtained_marks').val();
    if(validate_marks()){
        obtained_marks = input_value;
    }
});

function validate_marks(){
    var type = $('#types').val();
    var input_value = $('#obtained_marks').val();
    if(type == 'cgpa'){
        if(input_value>10 || input_value<0){
            $('#obtained_marks').val(obtained_marks);
            return false;
        }   
    }else if(type='percentage'){
        if(input_value>100 || input_value<0){
            $('#obtained_marks').val(obtained_marks);
            return false;
        }
    }else{
        $(this).val('');
        return false;
    }
    return true;
}